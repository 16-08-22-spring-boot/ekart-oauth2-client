package com.classpath.ekart;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EkartOauthClientApplication {

	public static void main(String[] args) {
		SpringApplication.run(EkartOauthClientApplication.class, args);
	}

}
